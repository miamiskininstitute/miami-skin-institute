Dr. Jegasothy provides a personalized approach to cosmetic dermatology. In fact, if you have a "difficult" aesthetic skin or asymmetry Dr. Jegasothy prides herself on fixing all aesthetic concerns and she will think tirelessly long after your visit. Call (305) 569-0067 for more information!

Address: 135 San Lorenzo Ave, STE 870, Coral Gables, FL 33146, USA

Phone: 305-569-0067

Website: https://miamiskininstitute.com
